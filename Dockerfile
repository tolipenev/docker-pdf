FROM debian:buster


# install packages and clean up
RUN apt-get update \
	&& apt-get install -y  \
	git pandoc \
	texlive-latex-extra texlive-latex-recommended texlive-latex-base \
	linkchecker poppler-utils \
	&& rm -rf /var/log/cache/apt
