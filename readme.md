docker-pdf
=============

To put the docker image into gitlab.com

```
# don't show your to anyone
export CI_TOKEN=asdfghjkl1234

# login to gitlabs docker hub
docker login -u gitlab-ci-token -p $CI_TOKEN registry.gitlab.com

# build and puch it
docker build -t registry.gitlab.com/moozer/pdf-ci .
docker push registry.gitlab.com/moozer/pdf-ci
```

To use it

add a line like this in `.gitlab-ci.yml`:

```
...
image: registry.gitlab.com/moozer/pdf-ci
...
```

gitlab.com has some very good reference material for docker+CI, e.g. [here](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#container-registry-examples)
