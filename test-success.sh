#!/bin/bash

set -e

TESTDIR=$(dirname $0)
WORKDIR="$TESTDIR"

cd $WORKDIR

PANDOC_CMD="pandoc --from markdown
       --to latex
       --out $WORKDIR/test2.pdf
       --pdf-engine pdflatex
       $TESTDIR/test2.md"

CONVTOHTML_CMD="/usr/bin/pdftohtml -s $WORKDIR/test2.pdf"
LINKCHECK_CMD="linkchecker -v --check-extern $WORKDIR/test2-html.html"

echo "running command: $PANDOC_CMD"
$PANDOC_CMD

echo "running command: $CONVTOHTML_CMD"
$CONVTOHTML_CMD

echo "running command: $LINKCHECK_CMD"
$LINKCHECK_CMD




